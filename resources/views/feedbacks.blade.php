<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
	<title></title>
	<style>
            html, body {
                background-color: #fff;
                color: black;
                font-family: 'Raleway', Century ;
                font-weight: 100;
                font-size: 20px;
                height: 100vh;
                padding-left: 20px;
                padding-top: 20px;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
</head>
<body>
	{!! $feedbacks->appends(['oc' => '1'])->render() !!}
	
<form method="get" action="{{route('show')}}">
		{{csrf_field()}}
<button type="submit" class="btn btn-success">Go back to asking field</button>
</form>
    
</body>
</html>