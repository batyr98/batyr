<!doctype html>

<html class="no-js" lang="">
<head>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Bime.kz</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
<!-- header top section -->
<section class="banner" role="banner">
  <header id="header">
    <div class="header-content clearfix"> <a class="logo" href=""><img src="images/logo.svg" alt=""></a>
      <nav class="navigation" role="navigation">

        <ul class="primary-nav">
          <li><a href="/delivery"> delivery</a></li>
          <li><a href="/rk"> QUESTIONS</a></li>
          <li><a href="a4.html"> myBasket</a></li>
        </ul>

        <form>
          <p id="searchit"><input id="searchit1" type="search" name="q" placeholder=" what you want?"> <input id="searchit2" type="submit" value="search"></p>
        </form>

      </nav>
      <a href="#" class="nav-toggle">Menu<span></span></a> </div>
  </header>
</section>
<!-- header top section --> 
<!-- header content section -->
<section id="hero" class="section ">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-sm-6 hero">
        <div class="hero-content">
         <img src="images/portfolio/hero.svg">
    </div>
  </div>
    </div>
  </div>
</section>

<section id="portfolio">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <hr class="section">
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 portfolio-item"> <a href="/work-details" class="portfolio-link">
        <div class="caption">
          <div class="caption-content">
            <h3>new in</h3>
            <h4>view all</h4>
          </div>
        </div>
        <img src="images/m1.jpg" class="img-responsive" alt=""> </a> </div>
      <div class="col-sm-6 portfolio-item"> <a href="/work-details" class="portfolio-link">
        <div class="caption">
          <div class="caption-content">
            <h3>men</h3>
            <h4>Branding</h4>
          </div>
        </div>
        <img src="images/man.jpg" class="img-responsive" alt=""> </a> </div>
      <div class="col-sm-6 portfolio-item"> <a href="/work-details" class="portfolio-link">
        <div class="caption">
          <div class="caption-content">
            <h3>women</h3>
            <h4>view all</h4>
          </div>
        </div>
        <img src="images/m3.jpg" class="img-responsive" alt=""> </a> </div>
      <div class="col-sm-6 portfolio-item"> <a href="/work-details" class="portfolio-link">
        <div class="caption">
          <div class="caption-content">
            <h3>kids</h3>
            <h4>view all</h4>
          </div>
        </div>
        <img src="images/baby.jpg" class="img-responsive" alt=""> </a> </div>
      <div class="col-sm-6 portfolio-item"> <a href="/work-details" class="portfolio-link">
        <div class="caption">
          <div class="caption-content">
            <h3>shoes</h3>
            <h4>view all</h4>
          </div>
        </div>
        <img src="images/portfolio/work-5.jpg" class="img-responsive" alt=""> </a> </div>
      <div class="col-sm-6 portfolio-item"> <a href="/work-details" class="portfolio-link">
        <div class="caption">
          <div class="caption-content">
            <h3>accessories</h3>
            <h4>view all</h4>
          </div>
        </div>
        <img src="images/portfolio/work-6.jpg" class="img-responsive" alt=""> </a> </div>
    </div>
  </div>
</section>


<!-- footer section -->
<footer class="footer">
  <div class="container">
    <div class="col-md-6 left">
      <h4>help & information</h4>
      <p> student dicscount 10% <a href="mailto:hello@agency.com"> </a></p>
      <p> delivery & returns<a href="mailto:hello@agency.com"> </a></p>
      <p> bime@gmail.com <a href="mailto:asylbekovbatyrzhan@gmail.com"></a></p>
    </div>
    <div class="col-md-6 right">
      <div class="about"> <a href="index.html">
      <p> about us </p>
      </a> </div>
      <div class="about1"> <a href="index.html">
        <p> careers at BIME </p>
      </a> </div>
      <div class="about2"> <a href="index.html">
        <p> - </p>
      </a> </div>
      <p>© 2018  BIME</p>
    </div>

  </div>
</footer>
<!-- footer section --> 

<!-- JS FILES -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.fancybox.pack.js"></script> 
<script src="js/retina.min.js"></script>
<script src="js/modernizr.js"></script> 
<script src="js/main.js"></script>
</body>
</html>
