<!doctype html>

<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bime.kz</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/a1.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
<!-- header top section -->
<section class="banner" role="banner">
    <header id="header">
        <div class="header-content clearfix"> <a class="logo" href="/"><img src="images/logo.svg" alt=""></a>
            <nav class="navigation" role="navigation">

                <ul class="primary-nav">
                    <li><a href="a1.html"> delivery</a></li>
                    <li><a href="a3.html"> sales</a></li>
                    <li><a href="a4.html"> myBasket</a></li>
                </ul>

                <form>
                    <p id="searchit"><input id="searchit1" type="search" name="q" placeholder=" what you want?"> <input id="searchit2" type="submit" value="search"></p>
                </form>

            </nav>
            <a href="#" class="nav-toggle">Menu<span></span></a> </div>
    </header>
</section>
<!-- header top section -->
<!-- header content section -->
<section id="hero" class="section ">
    <div class="container1">

            <div >

                    <h1>delivery</h1>
                <div class="line"></div>
                    <h2>free delivery</h2>
                    <p>you can order for free if you purchaised for 99$ and higher</p>
                    <p>usually shipping takes up to 4 weeks</p>
                    <p>after making purchase you will be given a track code, so you can see where your staff is.</p>
                <div class="line1"></div>
                <div>

                    <h2>9.99$ delivery</h2>
                    <p>you can order anything and pay only 9.99$ for delivery</p>
                    <p>usually shipping takes up to 4 weeks</p>
                    <p>after making purchase you will be given a track code, so you can see where your staff is.</p>
                </div>
                <div class="line1"></div>
                <div>

                    <h2>next day delivery</h2>
                    <p>you can order anything and pay only 29.99$ for delivery</p>
                    <p>usually shipping takes up to 1-3 days</p>
                    <p>after making purchase you will be given a track code, so you can see where your staff is.</p>
                </div>
             </div>
    </div>
</section>




<!-- footer section -->
<footer class="footer">
    <div class="container">
        <div class="col-md-6 left">
            <h4>help & information</h4>
            <p> student dicscount 10% <a href="mailto:hello@agency.com"> </a></p>
            <p> delivery & returns<a href="mailto:hello@agency.com"> </a></p>
            <p> bime@gmail.com <a href="mailto:asylbekovbatyrzhan@gmail.com"></a></p>
        </div>
        <div class="col-md-6 right">
            <div class="about"> <a href="a2.html">
                <p> about us </p>
            </a> </div>
            <div class="about1"> <a href="a2.html">
                <p> careers at BIME </p>
            </a> </div>
            <div class="about2"> <a href="a2.html">
                <p> - </p>
            </a> </div>
            <p left>© 2018  BIME</p>
        </div>

    </div>
</footer>
<!-- footer section -->

<!-- JS FILES -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/modernizr.js"></script>
<script src="js/main.js"></script>
</body>
</html>
