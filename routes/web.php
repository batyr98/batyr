<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	//return realpath(base_path('resources/views'));
    return view('index');
});

Route::get('/delivery', function () {
	//return realpath(base_path('resources/views'));
    return view('a1');
});
Route::get('/work-details', function () {
	//return realpath(base_path('resources/views'));
    return view('work-details');
});

Route::get('/rk', 'feedbackController@show')->name('show');
Route::post('/rk', 'feedbackController@store')->name('store');
Route::get('/feedbacks', 'feedbackController@show1')->name('show1');
Route::get('/', 'feedbackController@index')->name('index');
